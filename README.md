Valósíts meg egy Vue app-ot, ahol 2 felület van (login, dashboard), és nyelvváltási
lehetőség (en, hu) mindkét oldalon.

A nyelvváltás legyen a header-ben elhelyezve egy select-ként. A nyelvi változókat
JSON-ben tároljuk el. A nyelvváltás során az oldal nem tölthet újra.

A login oldalon egy egyszerű bootstrap-es form-ot kell megvalósítani (e-mail,
password), ahol bejelentkezéskor létrejön egy beégetett TOKEN pl.:
’SpjcYz8xQyY7w9Jz-DRU’, amit a localstorage-ban kell eltárolni, majd irányítson át az
app a Dashboard page-re.

A Dashboard pagen legyen egy táblázat, ami MOCK-olt user adatokat jelenít meg, és
egy kijelentkezés gomb.

A Dashboard page csak akkor nyitható meg, ha van TOKEN-ünk, egyéb esetben
irányítson vissza a Login page-re.