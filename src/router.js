import { createRouter, createWebHistory } from 'vue-router'
import Login from './components/Login.vue'

const routes = [
    {
        path: '/',
        name: 'Login',
        component: Login,
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: () => import('./pages/Dashboard.vue'),
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

router.beforeEach((to, from, next) => {
    if(to.path !== '/') {
        if(localStorage.getItem('token')) {
            next();
        } else {
            next('/');
        }
    } else {
        next();
    }
});

export default router