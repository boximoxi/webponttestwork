import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import "bootstrap/dist/css/bootstrap.css"
import i18n from './i18n'

import { createPinia } from 'pinia'

createApp(App)
    .use(router)
    .use(createPinia())
    .use(i18n)
    .mount('#app')

import "bootstrap/dist/js/bootstrap.js";